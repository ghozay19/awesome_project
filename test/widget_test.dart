// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:awesome_app/app/routes/route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

void main() {


  Widget testWidget(){
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.root,
      getPages: routes,
    );
  }

  testWidgets('test widget', (WidgetTester tester) async {

    final fab = find.byKey(ValueKey('fab'));
    final iconApp = find.byKey(ValueKey('appLogo'));

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget());
    // await tester.pumpWidget(SplashScreen());
    await tester.pump(Duration(seconds: 3));
    // await tester.pump();
    await tester.pumpAndSettle();

    expect(iconApp,findsOneWidget);
    // expect(find.byWidget(Image.asset(appLogo)), findsOneWidget);
    // // Verify that our counter starts at 0.
    // expect(find.text('0'), findsOneWidget);
    // expect(find.text('1'), findsNothing);
    //
    // // Tap the '+' icon and trigger a frame.
    // await tester.tap(find.byIcon(Icons.add));
    await tester.pump();
    //
    // // Verify that our counter has incremented.
    // expect(find.text('0'), findsNothing);
    // expect(find.text('1'), findsOneWidget);
    // expect(find.byKey(ValueKey('appLogo')),findsOneWidget);
    // expect(find.byWidget(Image.asset(appLogo)), findsOneWidget);
  });
}
