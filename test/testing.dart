


import 'package:awesome_app/app/data/network/repository/api_repository.dart';
import 'package:flutter_test/flutter_test.dart';

void main (){

  final repository = ApiRepository();

  test('load photos', () async {

    bool isLoaded;
    final response = await repository.doGetPhotoCurated(page: 1);

    if(response.photos==null){
      isLoaded = false;
    }else{
      isLoaded = true;
    }
    expect(isLoaded, true);

  });

}