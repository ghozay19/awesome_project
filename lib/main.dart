import 'dart:async';

import 'package:awesome_app/app/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';




void main() async {
  await mainProgram();
}


Future<void> mainProgram() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);


  runApp(AwesomeApp());
}