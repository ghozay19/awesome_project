import 'package:awesome_app/app/routes/route.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class AwesomeApp extends StatelessWidget {
  const AwesomeApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.root,
      getPages: routes,
    );
  }
}
