const apiKey = "563492ad6f9170000100000121d114319503435ca7128255c271aea0";

const String baseUrl = 'https://api.pexels.com/v1/';

String apiPhotoCurated(int page, int perPage) {
  return "/curated?page=${page.toString()}&per_page=${perPage.toString()}";
}
