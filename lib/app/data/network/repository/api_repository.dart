

import 'package:awesome_app/app/data/network/api_constants.dart';
import 'package:awesome_app/app/data/network/models/curated_photo_response.dart';
import 'package:awesome_app/app/data/network/service/service.dart';

class ApiRepository {

  final _apiService = ApiService(baseUrl);


  Future<CuratedPhotoResponse> doGetPhotoCurated(
      {required int page}) async {
    final response = await _apiService.call(apiPhotoCurated(page, 8),
        request: {},
        method: MethodRequest.GET,
      token: apiKey
    );
    if (response?.statusCode == 200) {
      return CuratedPhotoResponse.fromJson(response?.data);
    } else {
      return CuratedPhotoResponse();
    }
  }


}