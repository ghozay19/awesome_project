import 'package:awesome_app/app/ui/utils/debug_utils.dart';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';


enum MethodRequest { POST, GET, PUT, DELETE }

class ApiService {
  static Dio _dio = Dio();

  ApiService(String baseUrl) {
    _dio.options.baseUrl = baseUrl;
    _dio.options.connectTimeout = 90000; //90s
    _dio.options.receiveTimeout = 10000;
    _dio.options.validateStatus = (status) => true;
    _dio.options.headers = {
      'Accept': 'application/json',
    };
  }

  Future<Response?> call(String url, {MethodRequest method = MethodRequest.POST, required Map<String, dynamic> request, Map<String, String>? header, String? token, bool useFormData = false}) async {
    if (header != null) {
      _dio.options.headers = header;
    }
    if (token != null) {
      if (header != null) {
        header.addAll({
          'Authorization': '$token',

        });
        _dio.options.headers = header;
      } else {
        _dio.options.headers = {
          'Accept': 'application/json',
          'Authorization': '$token',
        };
      }
      if (method == MethodRequest.PUT) {
        _dio.options.headers = {'Accept': 'application/json', 'Authorization': '$token', 'Content-Type': 'application/x-www-form-urlencoded'};
      }
    }

    printDebug('URL : ${_dio.options.baseUrl}$url');
    printDebug('Header : ${_dio.options.headers}');
    printDebug('Request : $request');
    var selectedMethod;

    try {
      Response response;
      switch (method) {
        case MethodRequest.GET:
          selectedMethod = method;
          response = await _dio.get(
            url,
            queryParameters: request,
          );
          break;
        case MethodRequest.PUT:
          selectedMethod = method;
          response = await _dio.put(
            url,
            data: request,
          );
          break;
        case MethodRequest.DELETE:
          selectedMethod = method;
          response = await _dio.delete(
            url,
            data: useFormData ? FormData.fromMap(request) : request,
          );
          break;
        default:
          selectedMethod = MethodRequest.POST;
          response = await _dio.post(
            url,
            data: useFormData ? FormData.fromMap(request) : request,
          );
      }
      printDebug(response.data is Map);
      printDebug(response.data);

      if (response.data is Map) {
        printDebug('Success $selectedMethod $url: \nResponse : ${response.data}');
        return response;
      } else {
        printDebug(
            'Success NOT MAP $selectedMethod $url: \nResponse : ${response.data}');
        var error = Response(
            statusCode: 400,
            data: {
              'message': 'Terjadi kesalahan, coba lagi nanti',
              'result': false,
            },
            requestOptions: RequestOptions(path: ''));
        // return response;
        return error;
      }
    } on DioError catch (e) {
      var logger = Logger();
      logger.d('Request : $request \n $e');
      if (e.response?.data is Map) {
        (e.response?.data as Map).addAll(<String, dynamic>{
          "result": false,
        });
        return e.response;
      } else {
        var response = Response(data: {
          "message": "Terjadi kesalahan, coba lagi nanti",
          "result": false,
        }, requestOptions: RequestOptions(path: ''));
        return response;
      }
    }
  }
}