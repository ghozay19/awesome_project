import 'package:awesome_app/app/config/assets.dart';
import 'package:awesome_app/app/routes/route.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  @override
  void initState() {
    super.initState();
    _init();
  }

  Future _init() async {
    await Future.delayed(const Duration(seconds: 3));
    Get.toNamed(Routes.dashboard);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
                width: Get.width/3,
                height: Get.width/3,
                child: Image.asset(
                    appLogo,
                  key: Key('appLogo'),
                )),
          ),
          const Gap(18),
        ],
      ),
    );
  }
}
