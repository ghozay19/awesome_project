import 'package:awesome_app/app/config/colors.dart';
import 'package:awesome_app/app/data/network/models/photos.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher_string.dart';

class DetailScreen extends StatelessWidget {
  final Photos photos;
  const DetailScreen({Key? key,required this.photos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: primaryColor,
            expandedHeight: 500,
            // leading: BackButton(),
            leading: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                backgroundColor: secondaryColor,
                foregroundColor: Colors.white,
                child: BackButton(),
              ),
            ),
            flexibleSpace: FlexibleSpaceBar(
              titlePadding: EdgeInsets.zero,
              centerTitle: true,
              background: Hero(
                  tag: '${photos.id}',
                  child: CachedNetworkImage(
                    maxHeightDiskCache: 500,
                    useOldImageOnUrlChange: false,
                    imageUrl: '${photos.src?.original}',
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                    placeholder: (context, url) => Center(
                      child: Shimmer.fromColors(
                        baseColor: primaryColor,
                        highlightColor: secondaryColor,
                        child: Container(
                          height: 212,
                          alignment: Alignment.center,
                          child: SizedBox(
                            width: 0,
                          ),
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                    ),
                  )
              ),
            ),
          ),
          SliverList(delegate: SliverChildListDelegate([
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('${photos.photographer}'),
            ),
            InkWell(
              onTap: ()async{
                if (await canLaunchUrlString('${photos.photographerUrl}')) {
                await canLaunchUrlString('${photos.photographerUrl}');
                } else {
                throw 'Could not launch ${photos.photographerUrl}';
                }
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('${photos.photographerUrl}'),
              ),
            ),
          ]))
        ],
      ),
      );
  }
}
