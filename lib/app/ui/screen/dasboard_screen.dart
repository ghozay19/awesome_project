import 'package:awesome_app/app/modules/dashboard/dashboard_controller.dart';
import 'package:awesome_app/app/ui/screen/detail_screen.dart';
import 'package:awesome_app/app/ui/widget/network_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Get.find<DashboardController>();
    return Scaffold(
        floatingActionButton: Obx(() => FloatingActionButton(
          key: Key('fab'),
              onPressed: () {
                controller.changeLayout(controller.currentIndex);
              },
              child: Icon(
                controller.currentIndex == false ? Icons.grid_view : Icons.view_list,
              ),
              mini: true,
            )),
        body: Obx(() {
          if (controller.isLoadingFirst) return CircularProgressIndicator();
          if (controller.isEmpty) return Text('Empty');
          if (controller.isFailed) return Text('Failed');
          if (controller.listPhoto.isNotEmpty) {
            if (controller.currentIndex == false) {
              return LazyLoadScrollView(
                isLoading: controller.isLoading,
                onEndOfPage: controller.loadPerPage,
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    bool isItem = index < controller.count;

                    if (isItem) {
                      var data = controller.items[index];
                      return InkWell(
                        onTap: () => Get.to(
                          () => DetailScreen(
                            photos: data,
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Row(
                              children: [
                                NetworkImageWidget(
                                  data: data,
                                  width: 100,
                                  height: 100,
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${data.photographer}',
                                        ),
                                        Text(
                                          '${data.photographerUrl}',
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                    return Container();
                  },
                  itemCount: controller.count + 1,
                ),
              );
            } else {
              return LazyLoadScrollView(
                isLoading: controller.isLoading,
                onEndOfPage: controller.loadPerPage,
                child: MasonryGridView.count(
                  crossAxisCount: 2,
                  mainAxisSpacing: 4,
                  crossAxisSpacing: 4,
                  itemBuilder: (context, index) {
                    bool isItem = index < controller.count;

                    if (isItem) {
                      var data = controller.items[index];
                      return InkWell(
                        onTap: () => Get.to(
                          () => DetailScreen(
                            photos: data,
                          ),
                        ),
                        child: NetworkImageWidget(
                          data: data,
                        ),
                      );
                    }

                    return Container();
                  },
                  itemCount: controller.count + 1,
                ),
              );
            }
          }
          return Container();
        }));
  }
}
