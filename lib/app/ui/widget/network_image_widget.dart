import 'package:awesome_app/app/config/colors.dart';
import 'package:awesome_app/app/data/network/models/photos.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';

class NetworkImageWidget extends StatelessWidget {
  const NetworkImageWidget({
    Key? key,
    required this.data,
    this.width,
    this.height,
  }) : super(key: key);

  final Photos data;
  final double? width;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: '${data.src?.large}',
      fit: BoxFit.cover,
      height: height,
      width: width ?? MediaQuery.of(context).size.width,
      placeholder: (context, url) => Center(
        child: Shimmer.fromColors(
          baseColor: primaryColor,
          highlightColor: secondaryColor,
          child: Container(
            height: 212,
            alignment: Alignment.center,
            child: SizedBox(
              width: 0,
            ),
          ),
        ),
      ),
      errorWidget: (context, url, error) => Icon(
        Icons.error,
      ),
    );
  }
}
