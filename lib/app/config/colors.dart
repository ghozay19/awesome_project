import 'package:flutter/material.dart';

MaterialColor primarySwatch = Colors.blue;

Color primaryColor = Color(0xFFEAF6F8);
Color backgroundColor = Color(0xFFF9F9F9);
Color secondaryColor = Color(0xFF004751);