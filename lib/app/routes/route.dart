
import 'package:awesome_app/app/modules/dashboard/dashboard_binding.dart';
import 'package:awesome_app/app/ui/screen/dasboard_screen.dart';
import 'package:awesome_app/app/ui/screen/splash_screen.dart';
import 'package:get/get.dart';

class Routes {
  static const String root = '/';
  static const String dashboard = '/dashboard';


}
final List<GetPage> routes = [
  GetPage(
    name: Routes.root,
    page: () => SplashScreen(),
  ),
  GetPage(
    name: Routes.dashboard,
    page: () => DashboardScreen(),
    binding: DashboardBinding()
  ),
];
