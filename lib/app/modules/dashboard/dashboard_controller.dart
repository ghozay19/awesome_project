

import 'package:awesome_app/app/data/network/models/photos.dart';
import 'package:awesome_app/app/data/network/repository/api_repository.dart';
import 'package:awesome_app/app/ui/utils/debug_utils.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class DashboardController extends GetxController {

  var logger = Logger();
  final _repository = ApiRepository();
  final _isLoading = true.obs;
  final _isEmpty = false.obs;
  final _isFailed = false.obs;

  bool get isEmpty => _isEmpty.value;
  bool get isFailed => _isFailed.value;
  bool get isLoading => _isLoading.value;

  final _listPhoto = <Photos>[].obs;
  List<Photos> get listPhoto => _listPhoto;

  final _hasReachedMax = false.obs;
  final _page = 1.obs;
  int get page => _page.value;
  int get count => _listPhoto.length;
  bool get isFirstPage => page == 1;
  bool get isLoadingFirst => isLoading && isFirstPage;
  bool get isLoadingMore => isLoading && page > 1;
  bool get hasReachedMax => _hasReachedMax.value;
  List<Photos> get items => _listPhoto.toList();
  ItemScrollController itemScrollController = ItemScrollController();
  ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();


  final _isListLayout = false.obs;

  bool get currentIndex => _isListLayout.value;

  void changeLayout(bool index){
      _isListLayout.value = !index;
      printDebug(_isListLayout.value);
  }

  Future getPhotoCurated({bool isInitialFetching = false, bool isRefresh = false}) async {
    if (isInitialFetching == true) {
      _isLoading(true);
      _isFailed.value = false;
      _listPhoto.clear();
      _page.value = 1;
    }
    if (isRefresh == true) {
      _isFailed.value = false;
      _listPhoto.clear();
    }
    final data = await _repository.doGetPhotoCurated(page: 1);

    if (data.photos == null) {
      _isFailed.value = true;
    } else if (data.photos!.isEmpty) {
      _isEmpty.value = true;
    } else {
      _listPhoto.addAll(data.photos!);
    }
    _page.value = 2;
    _isLoading(false);
  }

  Future<void> loadPerPage() async {
    _isLoading(true);

    final data = await _repository.doGetPhotoCurated(page: page);

      if (data.photos!.isEmpty) {
        _isEmpty.value = false;
        _isFailed.value = false;
        _hasReachedMax.value = true;
      } else {
        _listPhoto.addAll(data.photos!);
        _isEmpty.value = false;
        _isFailed.value = false;
        _hasReachedMax.value = false;
        _page.value++;
      }

    _isLoading(false);
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    getPhotoCurated(isRefresh: true);
  }

}